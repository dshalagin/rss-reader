//
//  MXSAppDelegate.h
//  rss-reader
//
//  Created by Denis Shalagin on 09/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

@interface MXSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
