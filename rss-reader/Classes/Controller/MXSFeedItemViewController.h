//
//  MXSFeedItemViewController.h
//  rss-reader
//
//  Created by Denis Shalagin on 09/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

@class MXSFeedItem;

@interface MXSFeedItemViewController : UIViewController

//initializers
- (id)initWithFeedItem:(MXSFeedItem *)item; //designated initializer

@end
