//
//  MXSTableCell.m
//  rss-reader
//
//  Created by Denis Shalagin on 09/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "MXSTableCell.h"

@implementation MXSTableCell

#pragma mark - Static methods

+ (CGFloat)calculateHeightForWidth:(CGFloat)width withObject:(id)object {
    return 44.0f;
}

#pragma mark - Initialization

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    return [self initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
}

@end
