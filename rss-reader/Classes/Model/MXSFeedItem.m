//
//  MXSFeedItem.m
//  rss-reader
//
//  Created by Denis Shalagin on 09/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "MXSFeedItem.h"

@implementation MXSFeedItem

@synthesize title = _title;
@synthesize link = _link;
@synthesize text = _text;

#pragma mark - Initializaiton

- (void)dealloc {
    [_title release];
    [_link release];
    [_text release];
    
    [super dealloc];
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setTitle:[[self.title copyWithZone:zone] autorelease]];
        [copy setLink:[[self.link copyWithZone:zone] autorelease]];
        [copy setText:[[self.text copyWithZone:zone] autorelease]];
    }
    
    return copy;
}



@end
