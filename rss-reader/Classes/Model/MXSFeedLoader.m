//
//  MXSFeedLoader.m
//  rss-reader
//
//  Created by Denis Shalagin on 09/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "MXSFeedLoader.h"
#import "MXSFeedItem.h"

//elements
#define kElementItem @"item"
#define kElementTitle @"title"
#define kElementLink @"link"
#define kElementText @"description"

//----------------------------------------------------------------------
@interface MXSFeedLoader () <NSXMLParserDelegate>
@end

//----------------------------------------------------------------------
@implementation MXSFeedLoader {
    //data
    NSMutableArray *_foundItems;
    
    //temp objects
    MXSFeedItem *_currentItem;
    NSString *_currentElement;
    NSMutableString *_currentTitle;
    NSMutableString *_currentLink;
    NSMutableString *_currentText;
}

#pragma mark - Initialization

- (id)initWithDelegate:(id<MXSFeedLoaderDelegate>)delegate {
    self = [super init];
    if (self) {
        _delegate = delegate;
        _isLoading = NO;
    }
    
    return self;
}

#pragma mark - Public methods

- (void)loadData {
    _isLoading = YES;
    
    //load and parse data asynchronously
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
        NSURL *url = [[NSURL alloc] initWithString:@"http://images.apple.com/main/rss/hotnews/hotnews.rss"];
        NSXMLParser *xmlparser = [[NSXMLParser alloc] initWithContentsOfURL:url];
        [url release];
        
        xmlparser.delegate = self;
        
        _foundItems = [[NSMutableArray alloc] init];
        
        [xmlparser parse];
        [xmlparser release];
    });
}

#pragma mark - NSXMLParserDelegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    _currentElement = elementName;
    
    if ([elementName isEqualToString:kElementItem]) {
        _currentItem = [[MXSFeedItem alloc] init];

        _currentTitle = [[NSMutableString alloc] init];
        _currentLink = [[NSMutableString alloc] init];
        _currentText = [[NSMutableString alloc] init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if (!_currentItem) { //no item - no processing
        return;
    }
    
    if ([_currentElement isEqualToString:kElementTitle]) {
        [_currentTitle appendString:string];
    } else if ([_currentElement isEqualToString:kElementLink]) {
        [_currentLink appendString:string];
    } else if ([_currentElement isEqualToString:kElementText]) {
        [_currentText appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if ([elementName isEqualToString:kElementItem]) {
        //set parsed item values
        _currentItem.title = [NSString stringWithString:_currentTitle];
        _currentItem.link = [[_currentLink componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
        _currentItem.text = [NSString stringWithString:_currentText];
        
        //add item
        [_foundItems addObject:_currentItem];
        
        //reset temp values
        [_currentTitle release];
        [_currentLink release];
        [_currentText release];
        [_currentItem release];
        _currentItem = nil;
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    parser.delegate = nil;
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [self.delegate feedLoader:self didLoadItems:[NSArray arrayWithArray:_foundItems]];
        [_foundItems release];
        
        _isLoading = NO;
    });
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [self.delegate feedLoader:self errorLoadFeed:parseError];
        
        _isLoading = NO;
    });
}

@end
