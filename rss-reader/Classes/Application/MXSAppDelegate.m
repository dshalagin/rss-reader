//
//  MXSAppDelegate.m
//  rss-reader
//
//  Created by Denis Shalagin on 09/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "MXSAppDelegate.h"
#import "MXSFeedViewController.h"

@implementation MXSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //create window
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    self.window.backgroundColor = [UIColor whiteColor];
    
    //set root view controller
    UIViewController *rootViewController = [[MXSFeedViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
    
    [self.window setRootViewController:navigationController];
    
    [rootViewController release];
    [navigationController release];
    
    //show window
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
