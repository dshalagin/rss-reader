//
//  MXSTableCell.h
//  rss-reader
//
//  Created by Denis Shalagin on 09/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

@interface MXSTableCell : UITableViewCell

//properties
@property (nonatomic, strong) id object;

//static methods
+ (CGFloat)calculateHeightForWidth:(CGFloat)width withObject:(id)object;

//initializers
- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier; //designated initializer

@end
