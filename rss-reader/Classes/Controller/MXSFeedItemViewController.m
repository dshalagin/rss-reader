//
//  MXSFeedItemViewController.m
//  rss-reader
//
//  Created by Denis Shalagin on 09/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "MXSFeedItemViewController.h"
#import "MXSFeedItem.h"

@interface MXSFeedItemViewController() <UIWebViewDelegate>

@end

@implementation MXSFeedItemViewController {
    //view
    UIWebView *_webView;
    UIActivityIndicatorView *_activityIndicatorView;
    
    //data
    MXSFeedItem *_item;
}

#pragma mark - Initialization

- (id)initWithFeedItem:(MXSFeedItem *)item {
    self = [super init];
    if (self) {
        _item = [item copy];
    }
    
    return self;
}

- (void)dealloc {
    [_item release];
    
    [super dealloc];
}

#pragma mark - View lifecycle

- (void)loadView {
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    
    _webView = [[UIWebView alloc] initWithFrame:CGRectZero];
    _webView.scalesPageToFit = YES;
    _webView.delegate = self;
    [view addSubview:_webView];
    [_webView release];
    
    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [view addSubview:_activityIndicatorView];
    [_activityIndicatorView release];
    
    self.view = view;
    [view release];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = _item.title;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [_activityIndicatorView startAnimating];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //load link
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_item.link]]];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    _webView.frame = self.view.bounds;
    _activityIndicatorView.center = self.view.center;
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [_activityIndicatorView stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [_activityIndicatorView stopAnimating];
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                message:[error localizedDescription]
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                      otherButtonTitles:nil]
     show];
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
