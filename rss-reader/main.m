//
//  main.m
//  rss-reader
//
//  Created by Denis Shalagin on 09/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MXSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MXSAppDelegate class]));
    }
}
