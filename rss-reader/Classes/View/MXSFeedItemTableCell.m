//
//  MXSFeedItemTableCell.m
//  rss-reader
//
//  Created by Denis Shalagin on 09/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "MXSFeedItemTableCell.h"
#import "MXSFeedItem.h"

#define kOffset 20.0f
#define kTextLinesNumber 0 //show full title
#define kDetailLinesNumber 0 //show full description
#define kTextFont [UIFont boldSystemFontOfSize:16.0f]
#define kDetailFont [UIFont systemFontOfSize:14.0f]

@implementation MXSFeedItemTableCell

#pragma mark - Static methods

+ (CGFloat)calculateHeightForWidth:(CGFloat)width withObject:(id)object {
    assert([object isKindOfClass:[MXSFeedItem class]]);
    
    MXSFeedItem *item = (MXSFeedItem *)object;
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(kOffset, 0, width - (2 * kOffset), 0)];
    textLabel.numberOfLines = kTextLinesNumber;
    textLabel.font = kTextFont;
    textLabel.text = item.title;
    [textLabel sizeToFit];
    
    UILabel *detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(kOffset, 0, width - (2 * kOffset), 0)];
    detailLabel.numberOfLines = kDetailLinesNumber;
    detailLabel.font = kDetailFont;
    detailLabel.text = item.text;
    [detailLabel sizeToFit];
    
    CGFloat height = kOffset + textLabel.height +  (kOffset / 2) + detailLabel.height + kOffset;
    
    [textLabel release];
    [detailLabel release];
    
    return height;
}

#pragma mark - Initialization

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.textLabel.numberOfLines = kTextLinesNumber;
        self.textLabel.font = kTextFont;
        
        self.detailTextLabel.numberOfLines = kDetailLinesNumber;
        self.detailTextLabel.font = kDetailFont;
    }
    
    return self;
}

#pragma mark - View lifecycle

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.textLabel.frame = CGRectMake(kOffset, kOffset, self.width - (2 * kOffset), 0);
    [self.textLabel sizeToFit];
    
    self.detailTextLabel.frame = CGRectMake(kOffset, self.textLabel.bottom + (kOffset / 2), self.width - (2 * kOffset), 0);
    [self.detailTextLabel sizeToFit];
}

#pragma mark - Properties

- (void)setObject:(id)object {
    [super setObject:object];
    
    assert([object isKindOfClass:[MXSFeedItem class]]);
    
    MXSFeedItem *item = (MXSFeedItem *)object;
    
    self.textLabel.text = item.title;
    self.detailTextLabel.text = item.text;
}

@end
