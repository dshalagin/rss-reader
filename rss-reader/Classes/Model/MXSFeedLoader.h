//
//  MXSFeedLoader.h
//  rss-reader
//
//  Created by Denis Shalagin on 09/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

@protocol MXSFeedLoaderDelegate;

//----------------------------------------------------------------------
@interface MXSFeedLoader : NSObject

@property (nonatomic, assign, readonly) BOOL isLoading;
@property (nonatomic, assign) id<MXSFeedLoaderDelegate> delegate;

//initializers
- (id)initWithDelegate:(id<MXSFeedLoaderDelegate>)delegate; //designated initializer

//methods
- (void)loadData;

@end


//----------------------------------------------------------------------
@protocol MXSFeedLoaderDelegate <NSObject>

@required
- (void)feedLoader:(MXSFeedLoader *)feedLoader didLoadItems:(NSArray *)items;
- (void)feedLoader:(MXSFeedLoader *)feedLoader errorLoadFeed:(NSError *)error;

@end
