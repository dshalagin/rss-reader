//
//  MXSFeedViewController.m
//  rss-reader
//
//  Created by Denis Shalagin on 09/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "MXSFeedViewController.h"
#import "MXSFeedItemTableCell.h"
#import "MXSFeedItemViewController.h"
#import "MXSFeedItem.h"
#import "MXSFeedLoader.h"

//----------------------------------------------------------------------
@interface MXSFeedViewController () <UITableViewDataSource, UITableViewDelegate, MXSFeedLoaderDelegate>
@end

//----------------------------------------------------------------------
@implementation MXSFeedViewController {
    MXSFeedLoader *_feedLoader;
    
    //view
    UITableView *_tableView;
    UIRefreshControl *_refreshControl;
    UIActivityIndicatorView *_activityIndicatorView;
    
    //data
    NSMutableArray *_items;
}

#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _feedLoader = [[MXSFeedLoader alloc] initWithDelegate:self];
        _items  = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)dealloc {
    [_feedLoader release];
    [_items release];
    
    [super dealloc];
}

#pragma mark - View lifecycle

- (void)loadView {
    UIView *view = [[UIView alloc] init];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [view addSubview:_tableView];
    
    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl addTarget:self action:@selector(actionRefresh) forControlEvents:UIControlEventValueChanged];
    [_tableView addSubview:_refreshControl];
    
    [_tableView release];
    [_refreshControl release];
    
    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [view addSubview:_activityIndicatorView];
    [_activityIndicatorView release];
    
    self.view = view;
    [view release];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Apple Hot News", nil);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadData];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    _tableView.frame = self.view.bounds;
    _activityIndicatorView.center = self.view.center;
}

#pragma mark - Actions

- (void)loadData {
    //load data
    if (![_feedLoader isLoading]) {
        //show loading indicator
        [_activityIndicatorView startAnimating];
        
        //start loader
        [_feedLoader loadData];
    }
}

- (void)actionRefresh {
    [self loadData];
}

#pragma mark - MXSFeedLoaderDelegate

- (void)feedLoader:(MXSFeedLoader *)feedLoader didLoadItems:(NSArray *)items {
    //remove loading indicators
    [_activityIndicatorView stopAnimating];
    [_refreshControl endRefreshing];
    
    //refresh items
    [_items removeAllObjects];
    [_items addObjectsFromArray:items];
    
    //reload table
    [_tableView reloadData];
}

- (void)feedLoader:(MXSFeedLoader *)feedLoader errorLoadFeed:(NSError *)error {
    [_activityIndicatorView stopAnimating];
    [_refreshControl endRefreshing];
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                message:[error localizedDescription]
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                      otherButtonTitles:nil]
     show];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"FeedItemCell";
    MXSFeedItemTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[MXSFeedItemTableCell alloc] initWithReuseIdentifier:CellIdentifier] autorelease];
    }
    
    cell.object = _items[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [MXSFeedItemTableCell calculateHeightForWidth:_tableView.width withObject:_items[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MXSFeedItem *feedItem = _items[indexPath.row];
    
    MXSFeedItemViewController *controller = [[MXSFeedItemViewController alloc] initWithFeedItem:feedItem];
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

@end
